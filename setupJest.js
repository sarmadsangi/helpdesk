/* eslint-env jest */
import 'whatwg-fetch'

global.mockFetchResponse = (data) => {
  global.window.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => data
    })
  )
}
