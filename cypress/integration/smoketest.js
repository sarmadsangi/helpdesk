/* global describe, it, cy */

describe('Helpdesk Widget', () => {
  it('navigate to guide details page', () => {
    cy.visit('/')
    cy.get('a[data-testid=articleLink]:first').click()

    // back to home page
    cy.get('a[data-testid=backButton]').click()
  })

  it('paginate', () => {
    cy.visit('/')

    // one of those annoying browser test things
    cy.wait(100)
    cy.get('a[data-testid=pageLink2]').click()
    cy.get('a[data-testid=pageLink1]').click()
  })
})
