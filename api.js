const express = require('express')
const fetch = require('node-fetch')
const cache = require('memory-cache')
const cors = require('cors')

const app = express()
const port = process.env.PORT
const cacheTTL = 60 * 60 * 1000

app.use(cors())
app.get('/articles', async (req, res) => {
  const { page = 1 } = req.query
  const cachedResponse = cache.get(page)
  if (cachedResponse) {
    return res.send(cachedResponse)
  }

  const response = await fetch(`https://support.zendesk.com/api/v2/help_center/en-us/articles.json?page=${page}`)
  const body = await response.json()
  const articles = body.articles.map(({ id, title, body }) => ({ id, title, body }))
  const pageResponse = {
    page: body.page,
    page_count: body.page_count,
    per_page: body.page,
    articles: articles
  }

  cache.put(page, pageResponse, cacheTTL)

  return res.send(pageResponse)
})

app.listen(port, () => console.log(`API is running on ${port}! 🚀🚀🚀`))
