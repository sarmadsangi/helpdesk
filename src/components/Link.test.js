/* eslint-env jest */
import Link from './Link'
const { JSDOM } = require('jsdom')

describe('Link', () => {
  it('renders Link with all props, ', () => {
    const { window } = new JSDOM(Link('To the moon', '/btc', 'testId'))
    const document = window.document

    expect(document.querySelector('a').textContent).toEqual('To the moon')
    expect(document.querySelector('a').href).toEqual('/btc')
    expect(document.querySelector('a').getAttribute('data-testid')).toEqual('testId')

    expect(document.body.innerHTML).toMatchSnapshot()
  })
})
