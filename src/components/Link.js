import styles from './Link.css'

export default (text, href, testId) => `
  <a data-testid="${testId}" href="${href}" class="${styles.link}">${text}</a>
`
