/* eslint-env jest */
import Paginator, { getLinksRange } from './Paginator'
const { JSDOM } = require('jsdom')

describe('Paginator', () => {
  it('renders Paginator with all links', () => {
    const { window } = new JSDOM(Paginator(24, 2, '#/page'))
    const document = window.document

    expect(document.querySelectorAll('.paginator > a').length).toEqual(5)
    expect(document.querySelector('.paginator > a').getAttribute('href')).toEqual('#/page/1')
    expect(document.body.innerHTML).toMatchSnapshot()
  })

  it('should hide previous arrow link when there is not page to goto', () => {
    const { window } = new JSDOM(Paginator(10, 1, '#/page'))
    const document = window.document

    expect(document.querySelector('.paginator > a[data-testid=previousLink]').className).toEqual('hidden')
  })

  it('should hide next arrow link when there is not page to goto', () => {
    const { window } = new JSDOM(Paginator(10, 10, '#/page'))
    const document = window.document

    expect(document.querySelector('.paginator > a[data-testid=nextLink]').className).toEqual('hidden')
  })

  it('getLinksRange should return valid pagination range', () => {
    const pageCount = 10
    let currentPage = 5

    expect(getLinksRange(pageCount, currentPage)).toEqual([4, 5, 6])

    currentPage = 1
    expect(getLinksRange(pageCount, currentPage)).toEqual([1, 2, 3])

    currentPage = 10
    expect(getLinksRange(pageCount, currentPage)).toEqual([8, 9, 10])
  })
})
