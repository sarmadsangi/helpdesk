/* eslint-env jest */
import Card from './Card'
const { JSDOM } = require('jsdom')

describe('Card', () => {
  it('renders Card with Title and Description', () => {
    const { window } = new JSDOM(Card('Title', 'Some Description'))
    const document = window.document

    expect(document.querySelector('.title').textContent).toEqual('Title')
    expect(document.querySelector('.description').textContent).toEqual('Some Description')

    expect(document.body.innerHTML).toMatchSnapshot()
  })
})
