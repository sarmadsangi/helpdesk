import styles from './Paginator.css'

// Generates Range when given pageCount and currentPage ie
// pageCount = 10
// currentPage = 5
// getLinksRange(pageCount, currentPage) will return [4, 5, 6]
export const getLinksRange = (pageCount, currentPage) => {
  const linksToShow = 3
  let offsetFromSelectedPage = 2

  if (currentPage <= 1) {
    offsetFromSelectedPage = 1
  } else if (currentPage >= pageCount) {
    offsetFromSelectedPage = 3
  }

  const pagesRange = Array.from({ length: linksToShow }, (_, counter) => {
    const page = counter + 1
    return page + (currentPage - offsetFromSelectedPage)
  })

  return pagesRange
}

export default (pageCount, selectedPage, routeName) => {
  const currentPages = getLinksRange(pageCount, selectedPage)
  const previousPageURL = `${routeName}/${selectedPage - 1}`
  const nextPageURL = `${routeName}/${selectedPage + 1}`

  const previousPageLink = `
    <a
      data-testid="previousLink"
      class="${selectedPage <= 1 ? styles.hidden : ''}"
      href="${previousPageURL}">
      ◄
    </a>
  `

  const nextPageLink = `
    <a
      data-testid="nextLink"
      class="${selectedPage >= pageCount ? styles.hidden : ''}"
      href="${nextPageURL}">
      ►
    </a>
  `

  return `
    <div class="${styles.paginator}">
      ${previousPageLink}
      ${currentPages.map(page => `
        <a
          data-testid="pageLink${page}"
          class="${page === selectedPage ? styles.selected : ''}"
          href="${routeName}/${page}"
        >
            ${page}
        </a>
      `).join('')}
      ${nextPageLink}
    </div>
  `
}
