import styles from './Loader.css'

export default () => `
  <div data-testid="loader" class="${styles.loader}"></div>
`
