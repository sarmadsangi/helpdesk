/* eslint-env jest */
import Loader from './Loader'
const { JSDOM } = require('jsdom')

describe('Loader', () => {
  it('renders, ', () => {
    const { window } = new JSDOM(Loader())

    expect(window.document.body.innerHTML).toMatchSnapshot()
  })
})
