import styles from './Card.css'

export default (title, description) => `
  <div class="${styles.container}">
    <div class="${styles.title}">${title}</div>
    <div class="${styles.description}">${description}</div>
  </div>
`
