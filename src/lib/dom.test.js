/* eslint-env jest */
import { render, stripTags } from './dom'
const { JSDOM } = require('jsdom')

describe('DOM helper', () => {
  describe('render', () => {
    it('should render html to specified node', () => {
      const { window } = new JSDOM('<div id="app"></div>')
      const document = window.document

      render('<div>abc</div>', document.querySelector('#app'))
      expect(document.querySelector('#app').innerHTML).toEqual('<div>abc</div>')
    })
  })

  describe('stripTags', () => {
    it('should remove all tags', () => {
      expect(stripTags('<div id="app">Hello! there</div>')).toEqual('Hello! there')
    })
  })
})
