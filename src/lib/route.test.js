/* eslint-env jest */
import { getHashSegments, getRoute, goto } from './route'

describe('route Helper', () => {
  it('getHashSegments should split hash into parts', () => {
    window.history.pushState({}, 'post', '/#post/12')
    expect(getHashSegments()).toEqual(['#post', '12'])
  })

  it('getRoute should get route name', () => {
    window.history.pushState({}, 'post', '/#post/12')
    expect(getRoute()).toEqual('#post')
  })

  it('getRoute should get route name', () => {
    window.history.pushState({}, 'post', '/#post/12')

    goto('')
    expect(window.location.hash).toEqual('')
  })
})
