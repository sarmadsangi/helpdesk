export const render = (html, $node) => {
  $node.innerHTML = html
}

export const stripTags = (html) => {
  const div = document.createElement('div')
  div.innerHTML = html

  return div.textContent
}
