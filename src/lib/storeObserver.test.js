/* eslint-env jest */
import StoreObserver from './storeObserver'

class Store extends StoreObserver {}

describe('StoreObserver', () => {
  let store

  beforeEach(() => {
    store = new Store()
  })

  it('should subscribe to changes and gets notified when store changes', () => {
    const spyTicketChange = jest.fn()

    store.on('change:tickets', spyTicketChange)
    expect(spyTicketChange).toBeCalledTimes(1)
    expect(spyTicketChange).toBeCalledWith(undefined)

    store.set('tickets', { ticketId1: 'Ticket 1', ticketId2: 'Ticket 2' })
    expect(spyTicketChange).toBeCalledTimes(2)
    expect(spyTicketChange).toBeCalledWith({ ticketId1: 'Ticket 1', ticketId2: 'Ticket 2' })
  })

  it('store.get should return updated value', () => {
    expect(store.get('say')).toEqual(undefined)
    store.set('say', 'what')
    expect(store.get('say')).toEqual('what')
  })

  it('should disable notifications on unsubscription', () => {
    const spyTicketChange = jest.fn()

    store.on('change:tickets', spyTicketChange)
    expect(spyTicketChange).toBeCalledTimes(1)

    store.off('change:tickets', spyTicketChange)
    store.set('tickets', { ticketId1: 'Ticket 1', ticketId2: 'Ticket 2' })
    expect(spyTicketChange).toBeCalledTimes(1)
  })

  it('should only disable notifications unsubscribed function', () => {
    const spyTicketChange = jest.fn()
    const spySomeOtherChange = jest.fn()

    store.on('change:tickets', spyTicketChange)
    store.on('change:tickets', spySomeOtherChange)

    store.off('change:tickets', spyTicketChange)

    store.set('tickets', { ticketId1: 'Ticket 1', ticketId2: 'Ticket 2' })
    expect(spySomeOtherChange).toBeCalledTimes(2)
    expect(spyTicketChange).toBeCalledTimes(1)
  })
})
