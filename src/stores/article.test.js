/* eslint-env jest */
/* global mockFetchResponse */

import ArticleStore from './article'

describe('ArticleStore', () => {
  let articleStore

  it('should fetch data on initialization', (done) => {
    mockFetchResponse({ page_count: 1, articles: [{ id: '1', title: 'article 1' }] })
    articleStore = new ArticleStore()

    expect(articleStore.isLoading()).toEqual(true)

    process.nextTick(() => {
      expect(articleStore.getPageCount()).toEqual(1)
      expect(articleStore.getArticles()).toEqual([{ id: '1', title: 'article 1' }])
      expect(articleStore.getArticle('1')).toEqual({ id: '1', title: 'article 1' })
      expect(articleStore.isLoading()).toEqual(false)

      done()
    })
  })

  it('getArticle should return null if no article found', () => {
    articleStore = new ArticleStore()
    expect(articleStore.getArticle('1')).toBeNull()
  })
})
