import StoreObserver from '../lib/storeObserver'

const API_HOST = process.env.API_HOST

export default class ArticleStore extends StoreObserver {
  constructor (page) {
    super(null)
    this.fetchPage(page)
  }

  fetchPage (page = 1) {
    this.set('loading', true)

    // this can be a service/articles eventually
    window.fetch(`http://${API_HOST}/articles?page=${page}`)
      .then(res => res.json())
      .then((body) => {
        this.set('pageCount', body.page_count)
        this.set('articles', body.articles)
        this.set('loading', false)
      })
  }

  getPageCount () {
    return this.get('pageCount')
  }

  getArticles () {
    return this.get('articles')
  }

  getArticle (articleId) {
    const articles = this.getArticles()
    if (!articles) {
      return null
    }
    return articles.find(article => `${article.id}` === articleId)
  }

  isLoading () {
    return this.get('loading')
  }
}
