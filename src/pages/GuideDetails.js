import styles from './GuideDetails.css'
import Link from '../components/Link'

export default (article) => `
  <div>
    ${Link('Back', '#', 'backButton')}
    <div class="${styles.page}">
      <h2 class="${styles.title}">${article.title}</h2>
      <div class="${styles.body}">${article.body}</div>
    </div>
  </div>
`
