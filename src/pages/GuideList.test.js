/* eslint-env jest */
/* global mockFetchResponse */

import GuideList, { getSelectedPage } from './GuideList'
import ArticleStore from '../stores/article'
const { JSDOM } = require('jsdom')

const articles = [
  {
    title: 'Guide Details Title',
    body: '<span>Some Text</span>'
  },
  {
    title: 'Guide Details Title 2',
    body: '<span>Some Text 2</span>'
  }
]

describe('GuideList', () => {
  it('renders GuideList with Title and Description', done => {
    mockFetchResponse({ page_count: 1, articles: articles })
    const articleStore = new ArticleStore()

    process.nextTick(() => {
      const { window } = new JSDOM(GuideList(articleStore))
      const document = window.document

      expect(document.querySelectorAll('.articleList > a').length).toEqual(articles.length)
      expect(document.body.innerHTML).toMatchSnapshot()

      done()
    })
  })

  it('getSelectedPage should return current selected page', () => {
    window.history.pushState({}, 'articles', '/#articles/12')
    expect(getSelectedPage()).toEqual(12)
  })

  it('GuideList page should show loader when loading', () => {
    mockFetchResponse({ page_count: 1, articles: articles })
    const articleStore = new ArticleStore()
    const { window } = new JSDOM(GuideList(articleStore))
    const document = window.document

    expect(document.querySelector('.articleList > [data-testid=loader]')).toBeTruthy()
  })
})
