import { stripTags } from '../lib/dom'
import { getHashSegments } from '../lib/route'
import styles from './GuideList.css'
import Card from '../components/Card'
import Paginator from '../components/Paginator'
import Loader from '../components/Loader'

export const getSelectedPage = () => {
  const hashParts = getHashSegments()
  const routeName = hashParts[0]

  if (routeName === '#articles') {
    return parseInt(hashParts[1])
  }

  return 1
}

const renderList = (articles) => `
  ${articles.map(article => `
    <a data-testid="articleLink" class="${styles.articleLink}" href="#article/${article.id}">
        ${Card(article.title, stripTags(article.body))}
    </a>
  `).join('')}
`

export default (articleStore) => {
  const articles = articleStore.getArticles()
  const pageCount = articleStore.getPageCount()
  const loading = articleStore.isLoading()

  return `
    <div class="${styles.page}">
      <div class="${styles.pageHeader}">
        <h2>Help Centre</h2>
        ${Paginator(pageCount, getSelectedPage(), `#articles`)}
      </div>
      <div class="${styles.articleList}">
        ${loading ? Loader() : renderList(articles)}
      </div>
    </div>
  `
}
