/* eslint-env jest */
import GuideDetails from './GuideDetails'
const { JSDOM } = require('jsdom')

const article = {
  title: 'Guide Title',
  body: '<span>Guide Description</span>'
}

describe('GuideDetails', () => {
  it('renders GuideDetails with Title and Description', () => {
    const { window } = new JSDOM(GuideDetails(article))
    const document = window.document

    expect(document.querySelector('.title').innerHTML).toEqual(article.title)
    expect(document.querySelector('.body').innerHTML).toEqual(article.body)
    expect(document.querySelector('a[data-testid=backButton]').textContent).toEqual('Back')

    expect(document.body.innerHTML).toMatchSnapshot()
  })
})
