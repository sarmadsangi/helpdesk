import { render } from './lib/dom'
import { goto, getRoute, getHashSegments } from './lib/route'

import GuideList from './pages/GuideList'
import GuideDetails from './pages/GuideDetails'
import ArticleStore from './stores/article'

const articleStore = new ArticleStore()
const $app = document.querySelector('#app')

export const renderApp = () => {
  const hashParts = getHashSegments()

  switch (getRoute()) {
    // details page route
    case '#article':
      const articleId = hashParts[1]
      const article = articleStore.getArticle(articleId)
      if (!article) {
        goto('#')
        break
      }

      render(GuideDetails(article), $app)
      break

    // all the other routes
    default:
      const page = hashParts[1]
      articleStore.fetchPage(page)

      articleStore.on('change:loading', () => {
        render(GuideList(articleStore), $app)
      })
      break
  }
}

renderApp()
window.onhashchange = () => renderApp()
