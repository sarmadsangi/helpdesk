**Visit [temp.sarmadsangi.com/example](http://temp.sarmadsangi.com/example/) to check the demo**

Demo html is located in `example` directory.

### Setup

Install it:

```bash
npm install
```

Start:

```bash
npm start
```

On successful start, you should see something like this,

```bash
API is running on 3000! 🚀🚀🚀
Server running at http://localhost:3001
✨  Built in 2.50s.
```

npm start would start both API (port 3000) and Widget (3001).

Tests:

runs all the unit tests

```bash
npm test
```

runs code converage report

```bash
npm run test:coverage
```

runs browser tests (bit flaky 😞)

```bash
npm run browser-tests
```

before running this, make sure to,
1. Install cypress - [cypress setup](https://docs.cypress.io/guides/getting-started/installing-cypress.html)
2. Have API and Widget UI running


### Architecture/Patterns/Tech choices

#### UI Components

Goal of this challenge was to build embeddable JS widget using vanilla JS. There are several ways of building UI using vanilla javascript. Most folks use DOM API to create elements and populate UI, and i have done that in past too but for this exercise i ended up using `Template literals` for templating components ie

```javascript
import styles from './Card.css'

export default (title, description) => `
  <div class="${styles.container}">
    <div class="${styles.title}">${title}</div>
    <div class="${styles.description}">${description}</div>
  </div>
`
```

Template literals are pretty cool and have been widely used for building dom. I am using this to create re-usable components in vanilla js. Here is one cool project around this concept [hyperHTML](https://github.com/WebReflection/hyperHTML).

This approach makes UI building fun but it does have few downsides,

1. From Security point of view, this is not very safe. Unsafe markup can easily make its way in. Before productionising this, i would put a little more effort here in either making this a proper template engine that can parse and validate html or just use a 3rd party library.

2. Performance. Libraries like hyperHTML do virtual diff to avoid unnecessary renders.

#### Observer Pattern

I wanted to keep UI and data flow as simple as possible. So i ended up building this tiny Observer class that different render functions can subscribe to. This made it super easy to listen to changes and re-render UI.

```javascript
store.on('change:somedata', renderMe)
```

#### Bundling/Dev workflow

I am using [Parcel.JS](https://parceljs.org/) for this project. Main reason for using this was to quickly get development environment running. Given limited time, i didn't thought that my time would be best spent setting up babel, webpack etc from scratch.

#### CSS modules

ParcelJS out of the box enables css modules using postcss plugin. CSS modules help with local css where we don't have to worry about mixing styles up. Remember BEM? Well its no more needed, each class name is auto suffixed with a hash to make sure class name collision do not happen.

